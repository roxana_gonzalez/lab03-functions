#include "xyplotwindow.h"
#include <QApplication>
#include <iostream>

using namespace std;

//**************************************************
// Esta función solo ilustra la diferencia entre   *
// hacer cambios en variables recibieron valores y *
// hacer cambios en variables de referencia        *
//**************************************************


void ilustracion(int paraValor, int &paraRef)
{
    paraValor=1;
    paraRef=1;
    cout << endl << "El contenido de paraValor es: " << paraValor << endl
         << "El contenido de paraRef es: " << paraRef << endl;
}

//************************************
// Definición de la función mariposa *
//************************************

// ESCRIBE AQUI TU FUNCION MARIPOSA


//***************
// Función main *
//***************


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    XYPlotWindow w;
    double y = 0.00;
    double x = 0.00;
    double increment = 0.01;
    int argValor=0, argRef=0;


   // Invocación a la función ilustracion para ver los contenidos de
   // las variables que se pasan como valor y como referencia.

    ilustracion(argValor,argRef);
    cout << endl << "El contenido de argValor es: " << argValor << endl
         << "El contenido de paraRef es: " << argRef << endl;



    for (double t = 0; t < 16*M_PI; t = t + increment)
    {

        // para calcular las coordenadas x,y de la mariposa
        double r = pow(sin(1.2 * t), 2.0) + pow(cos(6 * t), 3.0);
        x = 5 * r * cos(t);
        y = 10 * r * sin(t);


        // invocamos la función mariposa enviando como argumento t
        // y las variables x, y

        // ESCRIBE AQUI TU INVOCACION A LA FUNCION MARIPOSA

        // añadimos la x y la y como un punto más de la gráfica
        w.AddPointToGraph(x,y);
    }

    // Una vez hemos añadido todos los puntos graficamos
    w.Plot();

    w.show();

    return a.exec();
}
