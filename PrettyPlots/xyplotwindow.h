#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"

namespace Ui {
class XYPlotWindow;
}

class XYPlotWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit XYPlotWindow(QWidget *parent = 0);
    //void setupgraph();
    ~XYPlotWindow();
    void AddPointToGraph(double x, double y);
    void Plot();


private:
    Ui::XYPlotWindow *ui;
    QVector<double> XX, YY;
    QVector<double> XXN, YYN;    
};

#endif // MAINWINDOW_H
